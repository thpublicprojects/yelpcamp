var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    mongoose        = require("mongoose"),
    flash           = require("connect-flash"),
    passport        = require("passport"),
    LocalStrategy   = require("passport-local"),
    methodOverride  = require("method-override"),
    Campground      = require("./models/campground"),
    Comment         = require("./models/comment"),
    User            = require("./models/user"),
    seedDB          = require("./seeds");

var campgroundRoutes    = require("./routes/campgrounds"),
    commentRoutes       = require("./routes/comments"),
    indexRoutes         = require("./routes/index");

//connect mongoose to our database server - if your collection doesnt exist it will create it (it also automatically pluralizes the collection name)
var url = process.env.DATABSEURL || "mongodb://localhost/yelp_camp"
mongoose.connect(process.env.DATABASEURL);  //local env. variable
//mongoose.connect("mongodb://localhost/yelp_camp");  //local
//mongoose.connect("mongodb://xxxxxx:xxxxxx@ds151070.mlab.com:51070/yelpcamp");  //mlab

//tells express to serve the contents of the public directory
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/public")); 
app.use(methodOverride("_method"));
app.use(flash());

//no longer need to specify that the view files are .ejs
app.set("view engine", "ejs");

//add sample data to the db
//seedDB();

//passport
app.use(require("express-session")({
    secret: "this can be anything",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//middleware - called one every route to include the current logged in user
app.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
});

app.use(indexRoutes);
app.use(commentRoutes);
app.use(campgroundRoutes);

//tell express to listen for requests (start server) - 3000 locally, and PORT for heroku
app.listen(process.env.PORT || 3000, function() {
    console.log("Server started!");
});