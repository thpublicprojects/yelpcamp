var express = require("express");
var router = express.Router();
var Campground = require("../models/campground");
var Comment = require("../models/comment");
var middleware = require("../middleware"); //if you require a directory it will auto require the index.js contents

//create a new comment
router.get("/campgrounds/:id/comments/new", middleware.isLoggedIn, function(req, res) {
    //find campround with the provided id
    Campground.findById(req.params.id, function(err, campground) {
        if (err) {
            console.log(err);
        } else {
            //render new template here
            res.render("comments/new", {campground: campground});
        }
    });
});

//open the form to create a new comment
router.post("/campgrounds/:id/comments", middleware.isLoggedIn, function(req, res){
    //find campround with the provided id
    Campground.findById(req.params.id, function(err, campground){
        if (err) {
            console.log(err);
            res.redirect("/campgrounds");
        } else {
            //create a new comment
            Comment.create(req.body.comment, function(err, comment){
                if (err) {
                    req.flash("error", "Something went wrong creating the comment.");
                    console.log(err);
                } else {
                    //add user name and id to comment and save the commment
                    comment.author.id = req.user._id;
                    comment.author.username = req.user.username;
                    comment.save();

                    //associate comment to a campground
                    campground.comments.push(comment);
                    campground.save();
                    req.flash("success", "Successfully posted your comment.");
                    res.redirect("/campgrounds/" + campground._id);
                }
            });
        }
    });
});

//comment edit - display the edit form
router.get("/campgrounds/:id/comments/:comment_id/edit", middleware.checkCommentOwnership, function(req, res){
    Comment.findById(req.params.comment_id, function(err, foundComment) {
        if (err) {
            req.redirect("back");
        } else {
            res.render("comments/edit", {campground_id: req.params.id, comment: foundComment});
        }
    });    
});

//comment update - actually save the changes
router.put("/campgrounds/:id/comments/:comment_id", middleware.checkCommentOwnership, function(req, res){
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, updatedComment) {
        if (err) {
            req.redirect("back");
        } else {
            res.redirect("/campgrounds/" + req.params.id);
        }
    });  
});

//destroy a comment
router.delete("/campgrounds/:id/comments/:comment_id", middleware.checkCommentOwnership, function(req, res){
    Comment.findByIdAndRemove(req.params.comment_id, function(err){
        if (err) {
            res.redirect("back");
        } else {
            req.flash("success", "Your comment has been successfully deleted.");
            res.redirect("/campgrounds/" + req.params.id);
        }
    })
});

module.exports = router;