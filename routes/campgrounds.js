var express = require("express");
var router = express.Router();
var Campground = require("../models/campground");
var middleware = require("../middleware"); //if you require a directory it will auto require the index.js contents

//display all campgrounds
router.get("/campgrounds", middleware.isLoggedIn, function(req, res) {
    //get all campgrounds from mongodb
    Campground.find({}, function(err, allCampgrounds) {
        if (err) {
            console.log(err);
        } else {
            res.render("campgrounds/index", {campgrounds: allCampgrounds});
        }
    });
});

//create a new campground
router.post("/campgrounds", middleware.isLoggedIn, function(req, res){
    //get data from form and add to campgrounds table
    var name = req.body.name;
    var price = req.body.price;
    var image = req.body.image;
    var description = req.body.description;
    var author = {
        id: req.user._id,
        username: req.user.username
    };
    var newCampground = {name: name, image: image, description: description, author: author};
    
    //create a new campground and save to mongodb
    Campground.create(newCampground, function(err, newlyCreated) {
        if (err) {
            console.log(err);
        } else {
            res.redirect("/campgrounds");
        }
    });
});

//new - show the form
router.get("/campgrounds/new", middleware.isLoggedIn, function(req, res) {
    //render form for creating a new campground
    res.render("campgrounds/new");
});

//dispay a particular campground - must be at the end otherwise the 'new' route will route here!
router.get("/campgrounds/:id", function(req, res) {
    //find campround with the provided id
    Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground) {
        if (err) {
            console.log(err);
        } else {
            //render show template here
            res.render("campgrounds/show", {campground: foundCampground});
        }
    });
});

//edit - display the edit form
router.get("/campgrounds/:id/edit", middleware.checkCampgroundOwnership, function(req, res){
    //if we made it here, then we already checked campgound ownership/authorization
    Campground.findById(req.params.id, function(err, foundCampground){
         res.render("campgrounds/edit", {campground: foundCampground});
    });    
});

//update
router.put("/campgrounds/:id", middleware.checkCampgroundOwnership, function(req, res){
    //find and update the correct campground and then redirect to the show page
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground){
        if (err) {
            res.redirect("/campgrounds");
        } else {
            res.redirect("/campgrounds/" + req.params.id);
        }
    });
    //body contains the entire campground object with the updated data from the form
});

//destroy a campground
router.delete("/campgrounds/:id", middleware.checkCampgroundOwnership, function(req, res){
    Campground.findByIdAndRemove(req.params.id, function(err){
        if (err) {
            res.redirect("/campgrounds");
        } else {
            req.flash("success", "Your campground has been successfully deleted.");
            res.redirect("/campgrounds");
        }
    })
});

module.exports = router;