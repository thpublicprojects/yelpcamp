var mongoose = require("mongoose");
var Campground = require("./models/campground");
var Comment = require("./models/comment");

var data = [
    {
        name: "Salmon Creek", 
        image:"https://farm3.staticflickr.com/2048/2984976440_559713072f.jpg",
        description: "Tenderloin leberkas turducken pastrami, alcatra biltong ham rump jerky chuck corned beef pork loin shank t-bone venison. Turducken andouille chuck meatball. Ham filet mignon salami, rump t-bone chicken pork chop pork belly cupim pork loin pastrami ground round jowl picanha. Turkey pastrami boudin drumstick bacon, rump ground round beef shoulder brisket pig strip steak cupim. Hamburger bacon beef sirloin, shoulder leberkas shank venison. Turducken pork chop tenderloin shoulder fatback ground round doner kielbasa hamburger alcatra meatball cupim bacon burgdoggen beef ribs."
    },
    {
        name: "Granite Hill", 
        image:"https://farm3.staticflickr.com/2839/11407596925_a0f9f4abf0.jpg",
        description: "Tenderloin leberkas turducken pastrami, alcatra biltong ham rump jerky chuck corned beef pork loin shank t-bone venison. Turducken andouille chuck meatball. Ham filet mignon salami, rump t-bone chicken pork chop pork belly cupim pork loin pastrami ground round jowl picanha. Turkey pastrami boudin drumstick bacon, rump ground round beef shoulder brisket pig strip steak cupim. Hamburger bacon beef sirloin, shoulder leberkas shank venison. Turducken pork chop tenderloin shoulder fatback ground round doner kielbasa hamburger alcatra meatball cupim bacon burgdoggen beef ribs."
    },
    {
        name: "Mountain Goat's Rest", 
        image:"https://farm9.staticflickr.com/8322/7887662552_8667d69960.jpg",
        description: "Tenderloin leberkas turducken pastrami, alcatra biltong ham rump jerky chuck corned beef pork loin shank t-bone venison. Turducken andouille chuck meatball. Ham filet mignon salami, rump t-bone chicken pork chop pork belly cupim pork loin pastrami ground round jowl picanha. Turkey pastrami boudin drumstick bacon, rump ground round beef shoulder brisket pig strip steak cupim. Hamburger bacon beef sirloin, shoulder leberkas shank venison. Turducken pork chop tenderloin shoulder fatback ground round doner kielbasa hamburger alcatra meatball cupim bacon burgdoggen beef ribs."
    }
]

function seedDB() {
    //remove all campgrounds
    Campground.remove({}, function(err){
        if (err) {
            consol.log(err);
        }
        console.log("Removed Campgrounds.");

        //add a few campgrounds
        // data.forEach(function(seed){
        //     Campground.create(seed, function(err, campground){
        //         if (err) {
        //             console.log(err);
        //         } else {
        //             console.log("Added a campground.");
        //             //create a comment on each campground
        //             Comment.create(
        //                 {
        //                     text: "This place is great, but I wish there was wifi.",
        //                     author: "Tudor"
        //                 }, function(err, comment){
        //                     if (err) {
        //                         console.log(err);
        //                     } else {
        //                         campground.comments.push(comment);
        //                         campground.save();
        //                         console.log("Created a new comment.");
        //                     }
        //             });
        //         }
        //     });
        // });
    });

    //add a few comments
}

module.exports = seedDB;

