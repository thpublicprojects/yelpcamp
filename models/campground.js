var mongoose = require("mongoose");

//pattern for our data in mongodb
var campgroundSchema = new mongoose.Schema({
    name: String,
    price: String,
    image: String,
    description: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"    
        },
        username: String
    },
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Comment"
        }
    ]
});

//create an object which can be used to connect to that object in mongodb
module.exports = mongoose.model("Campground", campgroundSchema);