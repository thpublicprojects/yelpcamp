var mongoose = require("mongoose");

//pattern for our data in mongodb
var commentSchema = new mongoose.Schema({
    text: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"    
        },
        username: String
    }
});

//create an object which can be used to connect to that object in mongodb
module.exports = mongoose.model("Comment", commentSchema);